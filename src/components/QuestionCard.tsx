import React from 'react'
import { AnswerObject} from '../App';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark,faCheck } from '@fortawesome/free-solid-svg-icons'

type Props = {
  question: string;
  answers: string[];
  callback: (e: React.MouseEvent<HTMLButtonElement>) => void;
  userAnswer: AnswerObject | undefined;
  questionNr: number;
  totalQuestions: number;
  result: AnswerObject;
}


const QuestionCard: React.FC<Props> = ({ question, answers, callback, userAnswer, questionNr, totalQuestions,result }) => (
  
  <div>
    <p className="number">
      Question: {questionNr} / {totalQuestions}
    </p>
    <p dangerouslySetInnerHTML={{ __html: question }} />
      <div className="wrap-btn">{answers.map((answer) => (
        <div key={answer} className="btn">
        <button className='btn-answer' disabled={userAnswer ? true : false } value={answer} onClick={callback}>
          <span dangerouslySetInnerHTML={{ __html: answer }} />
        </button>
        {result && result.correctAnswer === answer && <FontAwesomeIcon icon={faCheck} size="sm" style={{color: "#0ff029",}} />}
        {result && result.correctAnswer !== answer && <FontAwesomeIcon icon={faXmark} size="sm" style={{color: "#e90707",}} />}
      </div>))}
      </div>
  </div>
)
export default QuestionCard;