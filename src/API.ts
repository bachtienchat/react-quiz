import { shuffleArray } from "./utils";

export type Question = {
  /*
  category: "Science: Mathematics"
  correct_answer: "100"
  difficulty: "medium"
  incorrect_answers: (3) ['10', '1,000', '1,000,000']
  question: "How many zeros are there in a googol?"
  type: "multiple"
  */
  category: string;
  correct_answer: string;
  difficulty: string;
  incorrect_answers: string[];
  question: string;
  type: string;
}

export type QuestionState = Question & { answers: string[] };

export enum Difficulty { //cannot change value
  EASY = "easy",
  MEDIUM = "medium",
  HARD = "hard"
}

export const fetchQuizQuestions = async (amount: number, difficulty: Difficulty) => {
  const endpoint = `https://opentdb.com/api.php?amount=${amount}&difficulty${difficulty}&type=multiple`;
  const data = await (await fetch(endpoint)).json();
  return data.results.map((question: Question) => (
    {
      ...question,
      answers: shuffleArray([...question.incorrect_answers, question.correct_answer]) //mảng xáo trộn
    }
  ))
};