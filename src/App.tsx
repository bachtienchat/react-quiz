import { useState } from 'react'
import './App.css'
import QuestionCard from './components/QuestionCard'
import { TOTAL_QUESTIONS } from "./constants"
import { QuestionState, Difficulty, fetchQuizQuestions } from './API'

export type AnswerObject = {
  question: string;
  answer: string;
  correct: boolean;
  correctAnswer: string;
}

function App() {
  const [loading, setLoading] = useState(false);
  const [questions, setQuestions] = useState<QuestionState[]>([]);
  const [number, setNumber] = useState(0);
  const [userAnswers, setUserAnswers] = useState<AnswerObject[]>([]);
  const [score, setScore] = useState(0);
  const [gameOver, setGameOver] = useState(true);
  const [result, setResult] = useState<AnswerObject[]>([]);

  const startTrivia = async () => {
    setLoading(true);
    setGameOver(false);
    console.log(fetchQuizQuestions(TOTAL_QUESTIONS, Difficulty.EASY));
    await fetchQuizQuestions(TOTAL_QUESTIONS, Difficulty.EASY).then(data => setQuestions(data));
    setScore(0);
    setUserAnswers([]);
    setNumber(0);
    setLoading(false);
  }

  const checkAnswer = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (!gameOver) { //false init
      const answer = e.currentTarget.value; //take the value if users click on button
      const correct = questions[number].correct_answer === answer; //take the current questions with correct answer
      if (correct) setScore(prev => prev + 1); //plus 1 point if users answer correctly
      const answerObj = {
        answer,
        correct,
        correctAnswer: questions[number].correct_answer,
        question: questions[number].question,
      }
      setUserAnswers(prev => [...prev, answerObj]);
      setResult(prev => [...prev, answerObj]);
      console.log(result)
    }
  }

  const nextQuestion = () => {
    const nextQuestion = number + 1;
    if (nextQuestion === TOTAL_QUESTIONS) {
      setGameOver(true);
    } setNumber(nextQuestion);
  }
  return (
    <div className="App">
      <h1>REACT QUIZ</h1>
      {gameOver || userAnswers.length === TOTAL_QUESTIONS ? (
        <button className="start" onClick={startTrivia}>Start</button>
      ) : null}
      {questions.length > 0 && <button className="re-start" onClick={startTrivia}>Restart</button>}
      {!gameOver && questions.length > 0 &&
        <p className="score">Score: {score}</p>}
      {loading ? <p>Loading Question...</p> : null}
      {!loading && !gameOver && (
        <QuestionCard
          questionNr={number + 1}
          totalQuestions={TOTAL_QUESTIONS}
          question={questions[number].question}
          answers={questions[number].answers}
          userAnswer={userAnswers ? userAnswers[number] : undefined}
          callback={checkAnswer}
          result={userAnswers[number]}
        />
      )}

      {!gameOver && !loading && userAnswers.length === number + 1 && number !== TOTAL_QUESTIONS - 1 ? (
        <button className="next" onClick={nextQuestion}>Next Question</button>
      ) : null}
    </div>
  )
}

export default App
